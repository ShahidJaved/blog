<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*** Registration Routes ***/
Route::get('/', 'PostController@index')->name('index');
Route::get('/register', 'RegisterationController@create');
Route::post('/register', 'RegisterationController@store');

/*** Login Routes ***/
Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');
Route::get('/profile', 'SessionsController@showProfile');

/*** Post Routes ***/
Route::get('/create', 'PostController@create');
Route::post('/stores', 'PostController@store');
Route::get('/{postId}', 'PostController@show')->name('home');
Route::get('/deletepost/{postId}','PostController@destroy');

/*** Comment Routes ***/
Route::post('/{post}/createcomment', 'CommentController@store');




