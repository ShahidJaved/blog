@if(count($errors))
    <div class="col-sm-12">
        <br>
        <div class="form-group alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li> {{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif