@extends('layouts.master')
@section('content')
    <div class="col-sm-8">
    <p><strong> {{ $post->title }} :  {{$post->created_at->diffForHumans()}} <br></strong>{{ $post->body }}</p>
    <div class="comment form-group">
        <ul class="list-group">
@foreach($post->comments as $comment)
            <li class="list-group-item">{{ $comment->comment }} <strong> : {{   $comment->created_at->toFormattedDateString()   }} </strong> </li>
@endforeach
        </ul>
    </div>
@if ( Auth::check())
    <div class="card">
        <div class="card-block">
            <form method="POST" action="/{{$post->id}}/createcomment">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="comment">Comment</label>
                    <textarea  class="form-control" id="comment" name="comment" placeholder="Enter your Comment" required></textarea>
                </div>
                <button class="btn btn-primary pull-right">Add Comment</button>
            </form>
         </div>
    </div>
@endif
    </div>
    @include('layouts.sidebar')
@endsection
