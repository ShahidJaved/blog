@extends('layouts.master')
@section('content')
   <div class="col-sm-8">
        <p class="lead blog-description">You can create your blog here</p>
        <form method="POST" action="/stores">
        {{    csrf_field()    }}
            <div class="form-group row">
                <label for="username" class="col-form-label">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title" required>
            </div>
            <div class="form-group row">
                <label for="Blog" class="col-form-label">Blog</label>
                    <textarea  class="form-control" id="body" name="body" placeholder="Enter your blog" required></textarea>
            </div>
            <div class="form-group row">
                <div class="offset-sm-2 col-sm-12">
                   <button class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
        </form>
   </div>
   @include('layouts.sidebar')
@include('errors.errors')
@endsection