@extends('layouts.master')
@section('content')
    <div class="col-sm-8 blog-main">
    {{--@if(Auth::check())--}}
    {{--<div class="blog-post">--}}
        {{--<a href="/create"><button type = "button" class="btn btn-primary pull-right ">Create Post</button></a>--}}
    {{--</div><!-- /.blog-post -->--}}
{{--@endif--}}

        <div class="posts">
        @foreach($posts as $post)

            @if(Auth::check())
            <a class="pull-right" href="/deletepost/{{$post->id}}" >Delete</a>
            <p class="pull-right">|</p>
            <a class="pull-right"  id="editpost" href="#" onclick="editPost()" >Edit </a
            <p> <a href="/{{$post->id}}"> <strong>{{ $post->title }} <br> </strong></a> <strong>{{  $post->user->email  }} :  {{ $post->created_at->toFormattedDateString() }} </strong><br>{{ $post->body }}</p>
            @endif
                <hr>
        @endforeach
    </div>
    </div>
    @include('layouts.sidebar')
@endsection

