
 {{--* Created by PhpStorm.--}}
 {{--* User: shahidjaved--}}
 {{--* Date: 12/08/2017--}}
 {{--* Time: 1:36 PM--}}

<nav class="blog-nav">
    <a class="blog-nav-item active" href="/">Home</a>
    <a class="blog-nav-item" href="#">About</a>


@if ( Auth::check())
        <a class="blog-nav-item" href="/create">Create Post</a>
        <ul class="nav navbar-right dropdown">
            <li>
                <a aria-expanded="false" href="#" class="dropbtn" onclick="myFunction()">Welcome{{ ' '. ' '.Auth::user()->name }} <b class="caret"></b></a>
                <ul class="dropdown-content" id="myDropdown">
                    <li><a href="/profile">Profile</a></li>
                    <li><a href="/session">Session Info</a></li>
                    <li><a href="/logout"> logout</a></li>
                </ul>
            </li>
        </ul>
    @else
    <a class="blog-nav-item pull-right" href="/register">Register</a>
    <a class="blog-nav-item pull-right" href="/login">Login</a>
    @endif
</nav>


 @section('scripts')
 <script>
     /* When the user clicks on the button,
     toggle between hiding and showing the dropdown content */
function myFunction()
{
 document.getElementById("myDropdown").classList.toggle("show");
}

     // Close the dropdown menu if the user clicks outside of it
window.onclick = function(event)
{
    if (!event.target.matches('.dropbtn'))
    {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        for (var i = 0; i < dropdowns.length; i++)
        {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show'))
            {
                openDropdown.classList.remove('show');
            }
        }
    }
}
 </script>
 @endsection
