@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-sm-12">
                <h2>Profile</h2>
                <table class="table table-bordered">
                    <tr>
                        <td class="col-sm-3">Name</td>
                        <td class="col-sm-3">Email</td>
                        <td class="col-sm-3"> Action </td>
                    </tr>
                    <tr>
                        @foreach($profile as $profiledata=>$value)
                            <td> {{$value->name}}</td>
                            <td> {{$value->email}}</td>
                            {{--<td class="col-sm-3">{{$profile->name}}</td>--}}
                        @endforeach
                    </tr>
                    <tr>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@stop