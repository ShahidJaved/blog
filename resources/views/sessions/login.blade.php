@extends('layouts.master')
@section('content')
    <div class="col-sm-8 col-sm-offset-2">
        <form action="/login" method="POST">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="col-form-label">Name</label>
                    <input type="text" class="form-control" name="email" id="email" placeholder="Enter your name" required>
                </div>
                <div class="form-group">
                    <label for="name" class="col-form-label">Password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter your Password">
                </div>
            <div class="form-group">
                <button class="btn btn-primary pull-right">Login</button>
            </div>
        </form>
    </div>
@include('errors.errors')
@endsection

