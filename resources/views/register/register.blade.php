@extends('layouts.master')
@section('content')
    <div class="col-sm-8 col-sm-offset-2">
        <h3> Register </h3>
        <form action="/register" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="user_name" class="col-form-label">User Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Enter your user name">
            </div>

            <div class="form-group">
                <label for="email" class="col-form-label">Email</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Enter your email">
            </div>

            <div class="form-group">
                <label for="password" class="col-form-label">Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Enter your password">
            </div>

            <div class="form-group">
                <label for="confirm_password" class="col-form-label">Confirm Password</label>
                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Enter your password Again">
            </div>

            <div class="form-group">
                <button class="btn btn-primary pull-right"> Register</button>
            </div>
        </form>
    </div>
    @include('errors.errors')
@endsection
