<?php

namespace App;
use Illuminate\Database\Eloquent\Model as eloquent;

class Model extends eloquent
{
    protected $fillable=['title','body' , 'user_id' , 'post_id', 'comment'];
}
