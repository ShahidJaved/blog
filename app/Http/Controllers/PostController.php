<?php

namespace App\Http\Controllers;

use App\post;
use Illuminate\Http\Request;
use Carbon\Carbon;
class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function index()
    {
        if($user = \Auth::user())
        {
            $posts = post::latest()
                ->filter(request(['month','year']))
                ->where( 'user_id','=',  \Auth::user()->id )->get();

        }
        else
        {
            $posts = post::latest()
                ->filter(request(['month','year']))
                ->get();
        }
        return view('posts.index',compact('posts'));

    }
    public function show($postId)
    {
        $post = post::findorfail($postId);
        return view('posts.showsinglepost', compact('post'));
    }
    public function create()
    {
        return view('posts.create');
    }
    public function store(Request $request)
    {
        $this->validate(request(),
            [   'title' =>'required | min:2',
                'body' => 'required | min:3 '
            ]);

        auth()->user()->publish(
            new Post(request(['title','body']))
        );

        return redirect('/');
    }

    public function destroy($postId)
    {
        post::deletePost($postId);
        return redirect('/');
    }
}
