<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class registerationController extends Controller
{
    public function create()
    {
        return view('register.register');
    }

    public function store( User $user)
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|confirmed|min:1',
            'password_confirmation' => 'required'
        ]);
        $password = $user->setPassword(request('password'));

        try
        {
            $user = User::create([
                'name' => request('name'),
                'email' => request('email'),
                'password' => $password->password]);
        }
        catch(\Illuminate\Database\QueryException $e)
        {
            return redirect('/register')->withErrors(
                [
                    "message" => "Your email already exists"
                ]);
        }
            auth()->login($user);
            return redirect('/');

        }
}