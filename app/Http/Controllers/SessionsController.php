<?php

namespace App\Http\Controllers;

use App\post;
use App\User;
use Illuminate\Http\Request;

class sessionsController extends Controller
{
    public function create()
    {
        return view('sessions.login');
    }

    public function store()
    {
        if(auth()->attempt(['email' => request('email'), 'password' => request('password')]))
        {
            return redirect('/');
        }

        return back()->withErrors(
        [
            'message' => ' Please log back with Valid Credentials'
        ]);
    }
    public function showProfile(Request $request)
    {
        $profile  = user::where('email','=', \Auth::user()->email)->get();
        return view('sessions.profile', compact( 'profile' ));
    }

    public function destroy()
    {
        auth()->logout();
        return redirect('/');
    }
}
