<?php

namespace App;

use Carbon\Carbon;
class post extends Model
{
//    use softDeletes;
    protected $dates = ['deleted_at'];
    public function comments()
    {
        // A post can have many comments
        return $this->hasMany(comment::class);
    }

    public function addComment($comment_body)
    {
        return $this->comments()->create([
            'user_id' => auth()->user()->id,
            'comment' => $comment_body]);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeFilter($query, $filters)
    {
        if($month = $filters['month'])
        {
            $query->whereMonth('created_at',Carbon::parse($month)->month);
        }

        if( $year = $filters['year'])
        {
            $query->whereYear('created_at', $year);

        }
    }


    public static function archives()
    {
      if(\Auth::user())
      {
          return static::selectRaw('year(created_at) as year, monthname(created_at) as month, count(*) as published')
              ->groupBy('year','month')
              ->orderByRaw('min(created_at) desc')
              ->where('user_id','=',  \Auth::user()->id )->get()
              ->toArray();
      }
        return static::selectRaw('year(created_at) as year, monthname(created_at) as month, count(*) as published')
            ->groupBy('year','month')
            ->orderByRaw('min(created_at) desc')
            ->get()
            ->toArray();
    }

    public static function deletePost($postId)
    {
        post::destroy($postId);
    }
    
}
